#!/usr/bin/python
# ---------------------------------------------------------------
# fstab-automount.py (C) Copyright 2012 Daniel Fairhead
# python script for auto mounting multiple usb devices when
# they get plugged in.
# Either this can be run by udev, or else periodically.

import sys
import re
from subprocess import Popen, PIPE

# Generic 'run program, get output and errorcode' function:
def run(*args):
    """ runs a program, and returns (errorcode, output) """
    process = Popen(args, stdout=PIPE)
    output, _ = process.communicate()
    retcode = process.poll()
    return retcode, output.strip()

def run_only(*args):
    errcode, output = run(*args)
    if errcode:
        raise Exception('cannot run' + args[0] + '!')
    else:
        return output

# searches for KEY="value" in a string.  Returns value, or False
def search_for_pair(key, haystack):
    # search first with "double quotes"
    matches = re.search(key + '\s?=\s?"\s?([^"]*)\s?"', haystack, re.IGNORECASE)
    if not matches:
        # ok. well, try with 'single quotes'
        matches = re.search(key + "\s?=\s?'\s?(^'*)\s?'", haystack, re.IGNORECASE)

    if not matches:
        # ok, well, try without quotes.
        matches = re.search(key + "=(.*)", haystack, re.IGNORECASE)

    # Do we have a match?
    try:
        return matches.groups()[0]
    except:
        return False

#############################################################################
#                                                                           #
#  blkid (what devices are present, with UUIDs) parsing                     #
#                                                                           #
#############################################################################

def parse_blkid_line(line):
    to_return = {}

    # parse the line.
    device, details = line.split(':')

    # check for valid device:
    if device.startswith('/dev/'):
        # strip trailing ':'
        to_return['dev'] = device.strip()
    else:
        print 'Not a valid /dev/ device! %s' % device
        return False

    # search for label:
    to_return['label'] = search_for_pair('label', details)

    # search for uuid:
    to_return['uuid'] = search_for_pair('uuid', details)

    # search for fstype:
    to_return['type'] = search_for_pair('type', details)

    # add all details:
    to_return['details'] = details

    return to_return

def parse_blkid_lines(lines):
    accumulator = []
    for l in [ll.strip() for ll in lines]:
        if not l:
            continue
        if l.startswith('#'):
            continue
        parsed_dict = parse_blkid_line(l)
        if parsed_dict:
            accumulator.append(parsed_dict)
    return accumulator

def parse_blkid():
    blkid_output = run_only('blkid')
    return parse_blkid_lines(blkid_output.split('\n'))

#############################################################################
#                                                                           #
#  /etc/fstab (what devices can be mounted) parsing                         #
#                                                                           #
#############################################################################

def parse_fstab():
    places = []
    with open('/etc/fstab','r') as f:
        for line in [x.strip() for x in f.readlines()]:
            place = {}
            # ignore comments and empty lines
            if line.startswith('#') or not line:
                continue
            # these names come from the fstab manpage.
            place['spec'], place['file'],  place['vfstype'], \
            place['mntops'],  place['freq'], place['passno'] \
                = line.split()

            if place['file'].endswith('/'):
                place['file'] = place['file'].rstrip('/')
            places.append(place)
    return places

#############################################################################
#                                                                           #
#  What devices are present, but are not mounted currently?                 #
#                                                                           #
#############################################################################

def unmounted_present_drives():
    current_mounts = run_only('/bin/mount')
    fstab_mounts = parse_fstab()
    current_attached = parse_blkid()

    to_return = []

    for mountpoint in fstab_mounts:
        #print 'searching for ', mountpoint['file'], ' in ', current_mounts
        if current_mounts.find(mountpoint['file']) != -1:
            # print mountpoint['file'], 'is mounted'
            # it's already mounted.
            continue
        else:
            uuid = search_for_pair('UUID', mountpoint['spec'])
            if not uuid:
                uuid = mountpoint['spec']
            for attached in current_attached:
                if attached['uuid'] == uuid:
                    #print attached, ' is attached but not mounted'
                    to_return.append(mountpoint)

    return to_return

def mount_unmounted(sudo=True):
    for place in unmounted_present_drives():
        if sudo:
            run_only('sudo', 'mount', place['file'])
        else:
            run_only('mount', place['file'])

if __name__ == '__main__':
    command = sys.argv[-1]
    if command == 'list':
        for x in unmounted_present_drives():
            print x['file']
    elif command == 'mount':
        mount_unmounted()
    else:
        print 'Usage:'
        print 'list (list unmounted drives)'
        print 'mount (mount unmounded but present drives)'
